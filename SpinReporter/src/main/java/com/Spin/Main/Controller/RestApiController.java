package com.Spin.Main.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.Spin.Main.Dao.LoginDao;
import com.Spin.Main.Model.RequestWrapper;
import com.Spin.Main.Model.Signup;
import com.Spin.Main.Model.SuccessCode;
import com.Spin.Main.Repository.SignupRepository;
import com.Spin.Main.Services.SignupService;

@RestController
public class RestApiController {
//hi shiva
	@Autowired(required = true)
	private SignupService signupService;
	@Autowired
	private LoginDao login;
	
	@Autowired
	private SignupRepository signupRepository;

	@RequestMapping(value = "/api/LoginAPI/Login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<RequestWrapper> Login(@RequestBody Signup signup) {
		Signup signup1 = login.validateUser(signup);
		SuccessCode info = new SuccessCode();

		if (null != signup1) {
			RequestWrapper r = new RequestWrapper();
			info.setErrorCode(1);
			info.setErrorMessage("success");
			r.setInfo(info);

			// return Info;
			return new ResponseEntity<RequestWrapper>(r, HttpStatus.OK);

		} else {
			RequestWrapper r = new RequestWrapper();
			info.setErrorCode(0);
			info.setErrorMessage("failed to login");
			r.setInfo(info);
			// return Info;
			return new ResponseEntity<RequestWrapper>(r, HttpStatus.OK);

		}

	}

	@RequestMapping(value = "/api/RegistartionAPI/Signup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RequestWrapper> addEmployee(@Valid @RequestBody Signup signup) {
		
		Signup existingUser = signupRepository.findByEmailIgnoreCase(signup.getEmail());
		
		if(existingUser != null)
        {
			Signup s=new Signup();
			SuccessCode info = new SuccessCode();
			RequestWrapper r = new RequestWrapper();
			info.setErrorCode(2);
			info.setErrorMessage("Email Already Exists");
			/* info.setCurrentId(s.getId()); */
			r.setInfo(info);
			return new ResponseEntity<RequestWrapper>(r, HttpStatus.ALREADY_REPORTED);
        }

			else {
				if(signup.getPassword().equals(signup.getConfirmPassword())) {
			signupService.addUser(signup);
			SuccessCode info = new SuccessCode();
			RequestWrapper r = new RequestWrapper();
			info.setErrorCode(1);
			info.setErrorMessage("registration success");
			info.setCurrentId(signup.getId());
			r.setInfo(info);
			
			return new ResponseEntity<RequestWrapper>(r, HttpStatus.OK);
		}
			 else {
					SuccessCode info = new SuccessCode();
					RequestWrapper r = new RequestWrapper();
					info.setErrorCode(0);
					info.setErrorMessage("password and confirmPassword does not match");
					r.setInfo(info);

					return new ResponseEntity<RequestWrapper>(r, HttpStatus.NOT_ACCEPTABLE);

				}

			}
	}

	@RequestMapping(value = "/api/GetAPI/GetData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Signup> getdata(Model model) {

		return signupService.getUser();
	}

}
