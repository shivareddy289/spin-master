package com.Spin.Main.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.Spin.Main.Model.Signup;

@Repository
public class LoginDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public Signup validateUser(Signup signup) {
		String sql = "select * from signup where email='" + signup.getEmail() + "' and password='" + signup.getPassword()
				+ "'";
		List<Signup> users = jdbcTemplate.query(sql, new UserMapper());
		return users.size() > 0 ? users.get(0) : null;
	}
}

class UserMapper implements RowMapper<Signup> {
	public Signup mapRow(ResultSet rs, int arg1) throws SQLException {
		Signup user = new Signup();
		user.setEmail(rs.getString("email"));
		user.setPassword(rs.getString("password"));

		return user;
	}

	
}
