package com.Spin.Main.Model;

public class SuccessCode {
	
	private int errorCode;
	private String errorMessage;
	private int currentId;
	
	
	public SuccessCode() {
		super();
	}
	public SuccessCode(int errorCode, String errorMessage, int currentId) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.currentId = currentId;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getCurrentId() {
		return currentId;
	}
	public void setCurrentId(int currentId) {
		this.currentId = currentId;
	}
	
	

}
