package com.Spin.Main.Repository;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.Spin.Main.Model.Signup;

@Repository
@EnableAutoConfiguration
@EnableJpaRepositories
public interface SignupRepository extends JpaRepository<Signup, String> {
	
	Signup findByEmailIgnoreCase(String email);

}
