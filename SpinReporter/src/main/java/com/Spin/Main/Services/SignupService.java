package com.Spin.Main.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Spin.Main.Model.Signup;
import com.Spin.Main.Repository.SignupRepository;

@Service
public class SignupService {

	@Autowired
	private SignupRepository signupRepository;
	
	//private static List<Signup> users;
	public List<Signup> getUser() {
		List<Signup> list = new ArrayList<>();
		signupRepository.findAll().forEach(list::add);
		return list;

	}

	public void addUser(Signup signup) {
		signupRepository.save(signup);
	}
}
